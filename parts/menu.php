
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">




            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>

            <?php
                if(isset($_SESSION) && isset($_SESSION['lastname'])){
                    echo(' <li class="nav-item">
                <a class="nav-link" href="logout.php">Me déconnecter</a>
            </li>
              <li class="nav-item">
                <a class="nav-link" href="mon-compte.php">Mon compte !</a>
            </li>');
                } else {
                    echo('<li class="nav-item">
                <a class="nav-link" href="login.php">Me connecter</a>
            </li>');
                }
            ?>




        </ul>

        <?php
        if(isset($_SESSION) && isset($_SESSION['lastname'])  && isset($_SESSION['firstname'] )){
            echo('  <div class="float-end">
            <p>Bonjour ' . strtoupper($_SESSION['lastname']).' '. $_SESSION["firstname"].' </p>
        </div>');
        }
        ?>

    </div>
</nav>