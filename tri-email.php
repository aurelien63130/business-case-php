<?php
$adressesMail = [
    'aureliendelorme1@gmail.com',
    'welcome@gmail.com',
    'hello@supinfo.com',
    'aurelien.delorme@orange.fr',
    'test@yahoo.com',
    'bonjour@msn.com',
    'adelorme@humanbooster.com',
    'test@test.fr'
];

$total = count($adressesMail);

function emailByDomain($array){
    $domaines = [];
    foreach ($array as $mail){
        $string =  explode('@', $mail)[1];
        $string = explode(".", $string)[0];
        $domaines[] = $string;
    }

// Exemple 1 !
    $newArray = array_count_values($domaines);
    return $newArray;
}

var_dump(emailByDomain($adressesMail));

$domaines = [];
foreach ($adressesMail as $mail){
    $string =  explode('@', $mail)[1];
    $string = explode(".", $string)[0];
    $domaines[] = $string;
}

// Exemple 2

$domainesUniq = array_unique($domaines);

// On déclare un nouveau tableau dans lequel on ira stoquer nos clés valeurs domaine => nombre d'occurences
$resultat = [];

// Je parcours tous mes domaines uniques
foreach ($domainesUniq as $domain){
    // Je stock dans mon tableau une nouvelle clé qui correspond au nom du domaine
    $resultat[$domain]['nombre'] = 0;
    // Je parcours tous les domaines de mon tableau initial
    foreach ($domaines as $domaine){
        // Si jamais le domaine de mon tableau unique est égal au
        // domaine de tableau qui les contient tous (même les doublons)
        if($domaine == $domain){
            var_dump($domain);
            // A ce moment la j'incrémente mon compteur
            $resultat[$domain]['nombre']++;
            $resultat[$domain]['pourcentage']=  round(($resultat[$domain]['nombre']/$total)*100);
        }
    }
}
?>
<html>
<head>

</head>
<body>
<ol>
<?php
    foreach ($resultat as $key => $result){
        echo('<li>Nous avons '. $result['nombre']. ' adresse '
            . $key. ' ce qui représente '. $result['pourcentage']. ' %');
    }
?>
</ol>
</body>
</html>
