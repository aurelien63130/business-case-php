
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>

<?php
include 'parts/server-mod.php';
?>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">





            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
    </div>
</nav>

<h1>Panier !</h1>

<?php

$panier = [
    [
        'nom'=> 'Croquettes pour chien',
        'prix'=> 50,
        'quantité'=> 10,
        'image'=> 'https://www.carrefour.fr/media/540x540/Photosite/PGC/EPICERIE/3700260210191_PHOTOSITE_20210803_154814_0.jpg?placeholder=1'
    ],
    [
        'nom'=> 'Nourriture pour poisson',
        'prix'=> 1,
        'quantité'=> 2,
        'image'=> 'https://media.os.fressnapf.com/products/img/1001862/1001862_8829079574229.jpg?t=prod_xxs'
    ]
];

var_dump($panier);




$produit = [
    'nom'=> 'Croquettes pour chien',
    'prix'=> 50,
    'quantité'=> 10,
    'image'=> 'https://www.carrefour.fr/media/540x540/Photosite/PGC/EPICERIE/3700260210191_PHOTOSITE_20210803_154814_0.jpg?placeholder=1',
    'declinaisons'=> [
        'couleur'=> 'rouge'
    ]
];


$classementLigue1 = ['OM', 'Clermont Foot 63', 'Grenoble', 'ASSE', 'OL'];
echo('<ol>');
foreach ($classementLigue1 as $team){
    echo("<li>$team</li>");
}
echo('</ol>');


// BOUCLE FOR :
echo('<ol>');
for($i=0; $i< count($classementLigue1); $i++){
    echo("<li>".$classementLigue1[$i]."</li>");
}
echo('</ol>');



// Un tirage de nombre aléatoire compris entre 1 et 10 !

$tentative = 0;
$nombre = null;

while ($nombre != 3){
    $nombre = rand(1,10);
    $tentative++ ;
}

var_dump("Il a fallu ". $tentative .' tentatives');


// Dire combien de tentative il a fallu pour atteindre le nombre 3

?>

<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="<?php // echo($produit['image']); ?>" alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title"><?php // echo($produit['nom']); ?></h5>
        <p class="card-text">ETC ETC ... ! </p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>
</div>


<?php
include 'parts/footer.php';
?>

<script rel="script" src="scripts/bootstrap.bundle.min.js"></script>
</body>