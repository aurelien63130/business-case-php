
<html>
<head>
    <?php
    require 'product-functions.php';

    getOneById($_GET["id"]);

    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>

<?php
include 'parts/server-mod.php';
?>

<?php
    $produit = [
        "nom"=> "Croquettes pour chats",
        "description"=> "Croquettes super super bonnes",
        "image"=> "https://www.equilibre-et-instinct.com/536-thickbox_default/sachet-de-500g-de-croquettes-pour-chat-adulte.jpg",
        "note"=> 3,
        "commentaires" => [
            ["username"=> "Aurelien", "contenu"=> "MON CHAT A ADORE !!!"],
            ["username"=> "Un relou", "contenu"=> "Elles sont pas bonnes les croquettes !"]
        ]
    ];
?>



<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">



            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
<h1><?php
    echo('Article :' . $_GET["id"]);
    ?>!</h1>

    <div class="row">
        <div class="col-md-4  text-center">
            <img class="img-fluid" src="<?php echo($produit["image"]);?>">
        </div>

        <div class="col-md-2  text-justify"><?php echo($produit["description"]); ?></div>
    </div>

    <div class="row">
        <div class="col-md-4 color-yellow  text-center">

            <?php
             for($i = 0; $i<5; $i++){
                 if($produit["note"] > $i){
                     echo(" <i class=\"fas fa-star\"></i>");
                 } else {
                     echo('<i class="far fa-star"></i>');
                 }
             }
            ?>


        </div>
    </div>

    <div class="col-md-12 ">
        <div>
            <h2>Les commentaires</h2>
        </div>

        <?php
        foreach ($produit["commentaires"] as $commentaire){
            echo(' <div class="card mt-5">
            <div class="card-header">
                '.$commentaire["username"].'
            </div>
            <div class="card-body">
              
                <p class="card-text">'.$commentaire["contenu"].'</p>
            </div>
        </div>');
        }
        ?>
    </div>

</div>


<?php
include 'parts/footer.php';
?>

<script rel="script" src="scripts/bootstrap.bundle.min.js"></script>
</body>