<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';
    ?>
</head>
<body>
<?php
    include 'parts/server-mod.php';
?>
<?php


$variable = null;


$test = true;



// sinon on affiche pas booléan



$isAdmin = false;

$age = 2;



// Equivalant avec un switch


// Equi


if ($isAdmin) {
    echo('Tu es admin ! ');
}

// Assigne à la variable $displayErrors la valeur de la configuration PHP display_errors
$displayErrors = ini_get("display_errors");

// Assigne une nouvelle variable de type string avec la valeur Aurélien
$firstName = 'Aurélien';
// Assigne une nouvelle variable de type string avec la valeur Delorme
$lastName = 'Delorme';
// Nouvelle var de type booléan
$isConnected = 1;

// Vérifie si la variable displayErrors est égal à 1 ou '1'



if (!$isConnected) {
    // Redirection en PHP : On utilises :
    // header('Location: page_de_destination');
    header('Location: login.php');
}

$images = [
    [
        'text' => 'Tous les accessoires pour chien !',
        'url' => 'https://leparisien.fr/resizer/Ic7r3A1QUUrSgnmSqst4IibwvXA=/1200x675/cloudfront-eu-central-1.images.arcpublishing.com/leparisien/Q6MTNENGOZGU3BR5OUEO2GNMOI.jpg',
        'alt' => 'Image représentant un chien'
    ],
    [
        'text' => 'Tous les accessoires pour chat !',
        'url' => 'https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2FFAC.2Fvar.2Ffemmeactuelle.2Fstorage.2Fimages.2Fanimaux.2Fanimaux-pratique.2F7-qualites-du-chat-dont-on-peut-s-inspirer-pour-le-travail-46610.2F14838225-1-fre-FR.2F7-qualites-du-chat-dont-on-peut-s-inspirer-pour-le-travail.2Ejpg/850x478/quality/90/crop-from/center/7-qualites-du-chat-dont-on-peut-s-inspirer-pour-le-travail.jpeg',
        'alt' => 'Image représentant un chat !'
    ],
    [
        'text' => 'ASM !!!',
        'url' => 'https://www.sportbusinessmag.com/wp-content/uploads/2020/07/ASM-Clermont-Auvergne.jpg',
        'alt' => 'Un super logo !'
    ]
];



$produits =
    [
    [
    "nom"=> "Croquettes pour chats",
    "description"=> "Croquettes super super bonnes",
    "image"=> "https://www.equilibre-et-instinct.com/536-thickbox_default/sachet-de-500g-de-croquettes-pour-chat-adulte.jpg",
    "note"=> 3,
        "produit_flash"=> false
    ],
        [
            "nom"=> "Croquettes pour chien",
            "description"=> "Croquettes pas ouf !",
            "image"=> "https://www.cdiscount.com/pdt2/1/7/4/1/700x700/zca654174/rw/mini-croquettes-boeuf-pour-chien-1-kg-frolic.jpg",
            "note"=> 3,
            "produit_flash"=> true
        ]

    ];

    // Si vou avez le tri croissant en paramètre get vous affiches les produits du plus cher au moins cher

    require 'parts/menu.php'
?>




<h1>HomePage !</h1>



<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">

        <?php


            for ($i=0;$i<count($images);$i++){
                $active = '';
                if($i == 0){
                    $active = 'active';
                }
                echo('<button type="button" class="'.$active.'" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="'.$i.'" aria-label="Slide ' . $i+1 .'"></button>');
            }
        ?>

<!--        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>-->
<!--        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>-->
<!--        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>-->
    </div>
    <div class="carousel-inner">

        <?php
            foreach ($images as $key => $img){
                $active = '';
                if($key == 0){
                    $active = 'active';
                }

                echo('<div class="carousel-item '.$active.'">
            <img src="'.$img["url"].'" class="d-block w-100" alt="'.$img["alt"].'">
            <div class="carousel-caption d-none d-md-block">
                <h5>'.$img["text"].'</h5>
       
            </div>
        </div>');
            }
        ?>

    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>


    <div class="container"><h2>Les produits de mon BC ! </h2>


        <a href="table.php?order=price_desc">Tri par prix décroissant</a><br>
        <a href="table.php?order=price_asc">Tri par prix croissant</a>


        <div class="row">

            <?php
             foreach ($produits as $produit){
                 $iconeVenteFlash = '';
                 if($produit["produit_flash"]){
                     $iconeVenteFlash = '<i class="fab fa-free-code-camp"></i>';
                 }



                 echo('       <div class="card col-md-4">
                        <img class="card-img-top" src="'.$produit["image"].'" alt="Card image cap">
            <h5 class="card-header">'.$produit["nom"].' '.$iconeVenteFlash.'</h5>
            <div class="card-body">
            
            
                <h5 class="card-title">Special title treatment</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="product.php?nom='.$produit["nom"].'" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>');
             }
            ?>

        </div>
    </div>
</div>


<?php
    include 'parts/footer.php';
?>
<script rel="script" src="scripts/bootstrap.bundle.min.js"></script>
</body>


<?php

$sameDate = new DateTime('1993-03-30');
$date2 = new DateTime();
$diff = $sameDate->diff($date2);




$test = true;



unset($test);
unset($produits);

$arr = get_defined_vars();

//var_dump($arr);

echo(time());

var_dump(date("d/m/Y H:i"));


$date = new DateTime();
echo('Aujourd\'hui je vous envoie une certification SCRUM ! Nous sommes le '. $date->format('d/m/Y').'<br>');
$interval = DateInterval::createFromDateString('30 day');


$dateRendu = $date->add($interval);

echo('A rendre avant le : '. $dateRendu->format('d/m/Y'));


function additionner($nombre1, $nombre2)
{
    return $nombre1 - $nombre2;
}

$resultat = additionner(1,2);

var_dump($resultat);
?>