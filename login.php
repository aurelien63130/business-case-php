<html>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<?php
$errors = [];

$displayErrors = ini_get("display_errors");

$firstName = 'Aurélien';
$lastName = 'Delorme';
$isConnected = false;

if($_SERVER["REQUEST_METHOD"] == 'POST'){


    // L'email n'est pas saisie
    if(empty($_POST["email"])){
        $errors[] = 'Vous n\'avez pas saisi d\'adresse email';
    }

    // L'email n'est pas valide
    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $errors[] = "Ton mail n'est pas valide !";
    }

    // Il n'a pas saisie le mot de passe
    if(empty($_POST["password"])){
        $errors[] = 'Vous n\'avez pas saisie de password';
    }


    // Vérifier les identifiants de l'utilisateur
    if($_POST["email"] == 'aureliendelorme1@gmail.com' && $_POST["password"] == 'aurelien' && count($errors) == 0){

        $_SESSION["email"] = 'aureliendelorme1@gmail.com';
        $_SESSION["firstname"] = 'Aurélien';
        $_SESSION["lastname"] = 'Delorme';


        header('Location: index.php');
    } else {
       $errors[] = 'Les identifiants sont invalides !';
    }
    // Si ils sont bons, je redirige l'utilisateur
    // Sinon, je lui affiche un message
}

?>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="login.php">Se connecter !</a>
            </li>


            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
    </div>
</nav>

<h1>Formulaire de login !</h1>

<form action="login.php" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">

    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

    <?php
        foreach ($errors as $error){
            echo('<div class="alert alert-primary mt-3" role="alert">
                    '.$error.'
                   </div>');
        }
    ?>
</form>
<script rel="script" src="scripts/bootstrap.bundle.min.js"></script>
</body>