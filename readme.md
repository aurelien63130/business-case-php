# Recap 13 Janvier !

    - Site statique
      - Les données sont changées en manipulant les sources. Nous n'avons pas de base de donnée.
        
    - Client serveur 
        - Le client sera dans notre cas le navigateur 
        (envoie des requêtes HTTP pour réccupérer des données sur un serveur).
        - Le serveur renvoie des données au client (navigateur qui les affiche).

    - Apache 
      - Serveur web. 
      - Répond aux requêtes du navigateur en lui envoyant les ressources demandées
    - Mysql
      - Base données de type relationnel. 
        STRUCTURE : 
      - Schéma  
      - Tables qui contiennent des colonnes et des contraintes
      - On retrouvera des clés primaires 
      - On pourra retrouver des clés étrangères qui permettront de mettre en place les relations
    
    - PHP (Hypertext Preprocessor)
        - Executable installé sur le serveur web. Il permet d'executer des scripts PHP
      
    - Langage Serveur
        - Code source executé sur un serveur. Il permettra de développer un site web dynamique
            - Connexion utilisateur
            - Traitement des formulaires
            - Enregistrer des données en BDD. 
            - Un panier utilisateur sur un site e-commerce
    
      
    - Configuration PHP
        - La configuration PHP se situe dans le fichier php.ini 
        - On peut interagir avec depuis un éditeur de texte.
        
        - <?php ini_set('display_errors',0);?> modifie la valeur pendant la durée du script
        - <?php ini_get('display_errors');?> réccupére la valeur de la configuration actuelle

    - Développer une page php 
        - On cré un fichier avec l'extension .php
        - On se rend sur notre serveur web local (localhost:80 ?) suivi du chemin vers le fichier 
        - On peut maintenant utiliser les balises PHP  <?php ?> !

    - variables
        <?php
            // Déclaration d'une variable 
            $nomVar = 'valeur var';

            // Affiche la valeur de la variable 
            echo($nomVar);
        ?>

    - commentaire ! 
    
        <?php
        // Commentaire sur une seule ligne 
        
        /* TON 
            SUPER COMMENTAIRE SUR 
            PLUSIEURS LIGNES !
        /*
        ?>


        <!-- Commentaire en HTML --> 

        Les commentaires en PHP ne sont pas visible par le client contrairement à des commentaires en JS / HTML ...
        

    - Types de variables 
        <?php
            // Déclaration d'une nouvelle variable de type int
            $nombre = 10;
            // Déclaration d'une nouvelle variable de type string
            $prenom = 'Aurélien';
            // Déclaration d'une nouvelle variable de type booléen
            $idAdmin = true; 
            // float
            $price = 10.4; 
        ?>
    
    - Conditions

    - Structure : 
        <?php 
            $age = 18;
            $interditDeCasino = true;

            // Utiliation de l'instruction if
            if($age >= 18 && !$interditDeCasino ){
                echo('Tu peux rentrer !');
            } else if ($age >10 && !$interditDeCasino){ echo('Dans moins de 8 ans !); }
            else if ($interditDeCasino){
                echo('Tu n'as plus le droit :( !');
            }
            else {
                echo('Jamais');
            }

        ?>

    - Opérateurs de comparaison : 
        - == : vérifie une égalité sans vérifier son type    
        - === : Vérifie une égalité mais aussi le type
        - <= : Vérifier si une valeur était inférieur ou égal
        - < : Vérifier si une valeur était inférieur
         - >= : Vérifier si une valeur était supérieur ou égal
        - > : Vérifier si une valeur était supérieur
        - != : Vérifie une différence entre 2 valeurs
        - !$variable = négation de la variable ! Si = true, renvoie false
        
    
    #Recap 14 Janvier !
        - If / boolean

````php
<?php
    // Nouvelle variable de type boolean égale à true
    $boolean = true;
    // $boolean vaut maintenant true
    $boolean = !$boolean;
    
    if($boolean){
        // Si bool = à true, on execute ce code source
    }
    
        if(!$boolean){
        // Si bool = à true, on execute ce code source
    }
    
    
    
````
        - switch
````php
<?php
// ici : on test les valeurs que peut prendre la variable $i
switch ($i) {
    // Si elle est égal à 0 on passe dans ce cas 
    case $i:
        echo "i égal 0";
        // Le break permet de sortir du switch
        break;
    case 1:
        echo "i égal 1";
        break;
    case 2:
        echo "i égal 2";
        break;
}
?>
    
````
        - Ternaires

```php
<?php
    $number = 10;
    $variable =($number<100)?true:false;
    
?>
```
        - Tableaux
            Tableaux numéroté
```php
<?php
    // Créer un nouveau tableau numéroté vide 
    $tableau = [];
    // Equivalent 
    $tableau = array();
    
    // Créer un nouveau tableau numéroté avec des valeurs ! 
    $tableau = ['truc', 'machin', 'chose'];
    
    // Affiche le premier élément du tableau
    echo($tableau[0]);
    
   // Affiche tout le contenu de notre tableau
   var_dump($tableau);
   
   // Modifie le premier élément du tableau 
   $tableau[0] = 'Nouvelle valeur';
   // Notre nouveau tableau
   $tableau = ['Nouvelle valeur', 'machin', 'chose'];
    
   // Ajoute un élément dans le tableau
   $tableau[] = 'Nouvel élément' ;
   $tableau = ['Nouvelle valeur', 'machin', 'chose', 'Nouvel élément'];
?>
```
            Tableau associatif (clé valeur)
```php
<?php
    // Met en relation des clé et des valeurs.
    $tableau = [
        'cle'=> 'valeur',
        'autre_cle'=> 'autre valeur'
    ];
    
    // ICI $result sera égal à 'valeur'
    $result = $tableau["cle"];
?>    
```
    - Boucles
    - for / foreach
```php
<?php
    // Permet de faire une itération autant de fois que l'on souhaite
   for($start = 0; $start < 10; $i++ ){
    // $start prendra 0-1-2-3-4-5-6-7-8-9
   }
   $tableau = ['Croquettes', 'Harnais'];
   
   foreach ($tableau as $element){
        // La variable élément prendra la valeur Croquettes puis Harnais
   } 
   
   $identité = ['firstname'=>'Aurélien', 'lastname'=> 'Delorme'];
    
    
       
   foreach ($tableau as $key => $element){
        // La variable $element prendra la valeur Aurélien Delorme
        // La variable $key prendra la valeur firstname puis lastname 
   } 
   
 
   
?>    
```

    - while
```php
<?php
    // Affiche les nombres de 1 à 100
    while ($truc != 100){
        echo($truc);
        $truc ++;
    }
?>    
```
    
    - boucle infinie
    Remplace le chauffage

- Function
    évite la duplication de code source maintenance++ alléger le code
    Peux prendre une ou plusieurs données en entrée 
    Peux renvoyer des données en sortie 
  
````php
    // Prend $variable en entrée 
    // retourne un booléen
    $boolean = empty($variable);
````

    fonctions natives

    documentées sur
    https://www.php.net
    Il en existe beaucoup (tableau chaines de caractères etc etc ...)

```php

function additionner($int1, $int2){
   return $int1+$int2;
}

// Cette fonction retourne l'addition de 1 + 2  
$resultat = additionner(1,2);
````

- include : Permet de segmenter notre application integre un fichier à l'endroit ou il est appelé.
Si le fichier n'existe pas, un warning est envoyé à l'utilisateur.
  
- include_once : Si le fichier à dèjà été inclu, il ne le sera pas une deuxième fois. 
 
- require  : Stop le script avec une erreur si le fichier n'est pas trouvé. 

- require_once : Stop le script avec une erreur si le fichier n'est pas trouvé.
Vérifie aussi que le fichier n'a pas déjà été inclu


```php
include("chemin_vers_le_fichier.php");
include_once("chemin_vers_le_fichier.php");
require("chemin_vers_le_fichier.php");
require_once("chemin_vers_le_fichier.php");
```

# Recap 17 Janvier !

Variables Super globales ! 

 - GET : Permet d'envoyer des données dans notre URL // On Enverra un tableau clé => valeur

index.php?cle1=val1&cle2=val2
```php
    // Contient un tableau clé valeur avec tous les éléments dans l'URL
    var_dump($_GET);
    /* Affichera un tableau 
    [
        "cle1"=>"val1",
        "cle2"=> "val2"
    ]
```

- POST : On s'en sert dans les formulaires pour éviter d'envoyer toutes les données dans le l'URL
qui est limité en nombre de caractère + c'est pas beau

Avec la méthode POST les données ne sont plus envoyé dans les données mais dans le corps de la requête

````html
 <form method="post" action="login.php">
        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" name="username" class="form-control" id="username"  placeholder="Votre nom d'utilisateur !">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Mot de passe</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Mot de passe">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
````

Lorsque ce formulaire sera soumis, une requête de type POST sera envoyée sur la page login.php

On pourra alors réccupérer les valeurs dans un tableau clé => valeur

````php
        // JE CHECK QUE LA REQUETE EST DE TYPE POST 
        if($_SERVER["REQUEST_METHOD"] == 'POST'){
        // Je peux faire des vérifications sur mes variables
        if(empty($_POST["username"])){
            // ...
        }
        if(empty($_POST["password"])){
            // ...
        }
        }
````
Pour faire les redirections vous devez le faire avant d'avoir écrit du HTML sur la page. 

 - Session
Tableau clé valeur stocké sur le serveur (dans sa mémoire vive) qui est temporaire.
Elle a une durée de vie limitée
Elle est supprimé quand on utilise la fonction session_destroy
Si on redémarre le serveur web
   
````php
// Utilisation de la session 
session_start();

// ON PEUT LES AFFICHER, LES UTILISER ETC .. 
var_dump($_SESSION);

// SUPPRIME LA SESSION
session_destroy();
````



# Recap 18 Janvier !

- Cookies

Un cookie est un fichier texte envoyé au navigateur. Il est accessible modifiable etc ... Par l'utilisateur
Ils peuvent servir au fonctionnement du site ou à des fins commerciales. 
Ne pas oublier de faire accepter les cookie à l'utilisateur. 

Il est défini par une clé, une valeur est une durée de vie

Il sera aussi supprimé si l'utilisateur vide le cache de son navigateur. 

````php
<?php
setcookie("test", 'hello', time()+3600);  /* expire dans 1 heure */``

var_dump($_COOKIE);

?>
````

Upload d'image

Affichage d'un formulaire avec un upload d'image : 

- On indique la mèthode
- Ne pas oublier l'attribut HTML enctype qui doit être "multipart/form-data"
- On ajoute notre input de type file en oubliant pas son name !

````html
 <form method="post" action="index.php" enctype="multipart/form-data">
    <label for="uploadFile">Upload de fichier !</label>
    <input type="file" name="avatar">
    <input type="submit">
</form>
````

On oublie pas de sécuriser notre upload 
- On va vérifie le mimetype du fichier (extension) https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
- On va vérifier que le formulaire à été soumis (methode post) 
- On vérifier la taille du fichier 
- On génére un nom unique pour ne pas écraser nos fichiers

````php
if (isset($_FILES['avatar']) AND $_FILES['avatar']['error'] == 0) {
        // Testons si le fichier n'est pas trop gros
        if ($_FILES['avatar']['size'] <= 1000000)
        {
                 // Testons si l'extension est autorisée
                $infosfichier = pathinfo($_FILES['avatar']['name']);
                $extension_upload = $infosfichier['type'];
                $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
                if (in_array($extension_upload, $extensions_autorisees))
                {
                        // On peut valider le fichier et le stocker définitivement
                        move_uploaded_file($_FILES['avatar']['tmp_name'], 'uploads/' . basename($_FILES['avatar']['name']));
                        echo "L'envoi a bien été effectué !";
                } else {
                    echo('J\'accepte que les images ...');
                }
        } else {
            echo('le fichier est trop lourd pour un petit serveur ... ');
        }
}
````

la fonction move_uploaded_file 

https://www.php.net/manual/en/function.move-uploaded-file

Prend deux parametres. Déplace un fichier depuis le premier vers le deuxieme

Elle réccupére le premier paramètre qui est le lieu de stockage temporaire de notre upload 
$_FILES["avatar"]["tmp_name"]

Envoie le fichier dans le dossier que l'on souhaite (deuxième paramètre).


method action enctype

````php
move_uploaded_file($_FILES['avatar']['tmp_name'], 'uploads/' . basename($_FILES['avatar']['name']));
````


# Recap 19 Janvier !

- PDO 

Extension PHP qui permet de se connecter à notre BDD. 
Permet l'écriture de requête préparée (sécurise)
Permet de se connecter à différents SGBD (oracle, sqlserver, postgressql ... )

`````php
<?php
// On retrouve mysql on retrouve l'IP ou le nom de domaine de la base de donnée son nom, et l'encodage utilisa
// L'utilisateur et le mot de passe
$bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'user', 'password');


// Astuce try / catch : Permettez de visualiser les erreurs. 

try {
    $host = 'localhost';
    $dbName = ‘business-case‘;
    $user = 'root';
    $password = '';
    $pdo = new PDO(
        'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
        $user,
        $password);
    // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a 
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    throw new InvalidArgumentException('Erreur connexion à la base de données : '.$e->getMessage());
    exit;
}

// Requête get all 

 // Cré une requête de selection de tous no produits
 $selectAllProduit = $bdd->query('SELECT * FROM produit');
 // Réccupére nos resultats sous forme de tableau
 $results = $query->fetchAll()

/* Requête get one
    Permet de sécuriser les appels à notre bdd notament en évitant des attaques de type injection SQL 
*/
    $id = 1;
    // Je cré une requête qui selectionnera des produits en fonction de leur ID
    // C'est une requête préparée ou l'on a une variable (id)
    $query = $bdd->prepare('SELECT * FROM produit WHERE :id');  
    $query->execute(["id"=>$id]);  
?>
````
