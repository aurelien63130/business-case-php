
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php';

    $produits =
        [
            [
                "nom"=> "Croquettes pour chats",
                "description"=> "Croquettes super super bonnes",
                "image"=> "https://www.equilibre-et-instinct.com/536-thickbox_default/sachet-de-500g-de-croquettes-pour-chat-adulte.jpg",
                "note"=> 3,
                "prix"=> 10,
                "produit_flash"=> false
            ],
            [
                "nom"=> "Croquettes pour chien",
                "description"=> "Croquettes pas ouf !",
                "image"=> "https://www.cdiscount.com/pdt2/1/7/4/1/700x700/zca654174/rw/mini-croquettes-boeuf-pour-chien-1-kg-frolic.jpg",
                "note"=> 3,
                "prix"=> 100,
                "produit_flash"=> true
            ]
        ];

        if(isset($_GET["order"]) && $_GET["order"] == 'price_desc'){
            $columns = array_column($produits, 'prix');
            $products = array_multisort($columns, SORT_DESC, $produits);
        } elseif (isset($_GET["order"]) && $_GET["order"] == 'price_asc'){
            $columns = array_column($produits, 'prix');
            $products = array_multisort($columns, SORT_ASC, $produits);
        }
    ?>
</head>
<body>

<?php
include 'parts/server-mod.php';
?>




<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">



            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <h1>Les produits en BDD!</h1>


    <a href="table.php?order=price_desc">Tri par prix décroissant</a><br>
    <a href="table.php?order=price_asc">Tri par prix croissant</a>


    <div class="row">

        <?php
        foreach ($produits as $produit){
            $iconeVenteFlash = '';
            if($produit["produit_flash"]){
                $iconeVenteFlash = '<i class="fab fa-free-code-camp"></i>';
            }



            echo('       <div class="card col-md-4">
                        <img class="card-img-top" src="'.$produit["image"].'" alt="Card image cap">
            <h5 class="card-header">'.$produit["nom"].' '.$iconeVenteFlash.'</h5>
            <div class="card-body">
            
            
                <h5 class="card-title">'.$produit["prix"].'</h5>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <a href="product.php?nom='.$produit["nom"].'" class="btn btn-primary">Go somewhere</a>
            </div>
        </div>');
        }
        ?>

    </div>


</div>


<?php
include 'parts/footer.php';
?>

<script rel="script" src="scripts/bootstrap.bundle.min.js"></script>
</body>